#include "tiny-05.hpp"

namespace pjc {

    complex::complex(double real, double imaginary) :
            m_real(real),
            m_imag(imaginary) {}

    double complex::real() const {
        return m_real;
    }

    void complex::real(double d) {
        m_real = d;
    }

    double complex::imag() const {
        return m_imag;
    }

    void complex::imag(double d) {
        m_imag = d;
    }

    complex complex::operator+(const pjc::complex &obj) {
        double real = m_real + obj.real();
        double imag = m_imag + obj.imag();
        return {real, imag};
    }

    complex complex::operator-(const complex &obj) {
        double real = m_real - obj.real();
        double imag = m_imag - obj.imag();
        return {real, imag};
    }

    complex complex::operator*(const complex &obj) {
        double real = m_real * obj.real() - m_imag * obj.imag();
        double imag = m_real * obj.imag() + m_imag * obj.real();
        return {real, imag};
    }

    complex complex::operator+(const double &obj) {
        double real = m_real + obj;
        return {real, m_imag};
    }

    complex complex::operator-(const double &obj) {
        double real = m_real - obj;
        return {real, m_imag};
    }

    complex complex::operator*(const double &obj) {
        double real = m_real * obj;
        double imag = m_imag * obj;
        return {real, imag};
    }

    complex operator+(double lhs, const complex &rhs) {
        double real = lhs + rhs.real();
        return {real, rhs.imag()};
    }

    complex operator-(double lhs, const complex &rhs) {
        double real = lhs - rhs.real();
        double imag = rhs.imag() * -1;
        return {real, imag};
    }

    complex operator*(double lhs, const complex &rhs) {
        double real = lhs * rhs.real();
        double imag = lhs * rhs.imag();
        return {real, imag};
    }
}
