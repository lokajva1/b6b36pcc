#pragma once

#include <memory>

enum class generator_kind {
    random,
    mersenne,
    minstd
};


class numbers_generator {
public:

    double min_g_double = 0;
    double max_g_double = 0;
    size_t doubles_g = 0;
    int min_g_int = 0;
    int max_g_int = 0;
    size_t ints_g = 0;
    size_t trues_g = 0;
    size_t fasles_g = 0;
    size_t bools_g = 0;
    
    bool is_first_g_double = true;
    bool is_first_g_int = true;
    /**
     * Vygeneruje náhodné číslo v internumbu [0, 1)
     */
    double random_double() {
        doubles_g++;
        double numb = random_double_impl();
        if (is_first_g_double) {
            min_g_double = numb;
            max_g_double = numb;
            is_first_g_double = false;
        } else {
            if (numb < min_g_double) min_g_double = numb;
            if (numb > max_g_double) max_g_double = numb;
        }
        return numb;
    }
    /**
     * Vygeneruje náhodné celé číslo v internumbu [lower, upper]
     */
    int    random_int(int lower, int upper) {
        ints_g++;
        int numb = random_int_impl(lower, upper);
        if (is_first_g_int) {
            min_g_int = numb;
            max_g_int = numb;
            is_first_g_int = false;
        } else {
            if (numb < min_g_int) min_g_int = numb;
            if (numb > max_g_int) max_g_int = numb;
        }
        return numb;
    }
    /**
     * Vygeneruje náhodnou pravdivostní hodnotu (true/false)
     */
    bool   random_bool() {
        bools_g++;
        bool numb = random_bool_impl();
        numb ? trues_g++ : fasles_g++;
        return numb;
    }

    virtual ~numbers_generator() = default;

    static std::unique_ptr<numbers_generator> create_by_kind(generator_kind kind, size_t seed);

    double min_generated_double() const {return min_g_double;}
    double max_generated_double() const {return max_g_double;}
    size_t doubles_generated() const {return doubles_g;}
    int min_generated_int() const {return min_g_int;}
    int max_generated_int() const {return max_g_int;}
    size_t ints_generated() const {return ints_g;}
    size_t trues_generated() const {return trues_g;}
    size_t falses_generated() const {return fasles_g;}
    size_t bools_generated() const {return bools_g;}

private:
    virtual double random_double_impl() = 0;
    virtual int    random_int_impl(int lower, int upper) = 0;
    virtual bool   random_bool_impl() = 0;
};
