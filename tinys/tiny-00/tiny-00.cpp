#include "tiny-00.hpp"

#include <ostream>
#include <iostream>
#include <iomanip>
// don't forget to include appropriate headers

void write_stats(std::vector<double> const& data, std::ostream& out) {
    std::vector<double>::const_iterator it = data.begin();
    double average;
    double sum = 0;
    double roundNumb = 0;
    double min = *it;
    double max = *it;

    while (it != data.end()){
        if (*it < min){
            min = *it;
        }
        if (*it > max){
            max = *it;
        }
        sum += *it;
        roundNumb++;
        ++it;
    }

    average = sum / roundNumb;

    out << "min: " << std::fixed << std::setprecision(2) << min << std::endl
    << "max: " << std::fixed << std::setprecision(2) << max << std::endl
    << "mean: " << std::fixed << std::setprecision(2) << average << std::endl;
}
