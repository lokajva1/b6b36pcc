00 – Formátovaný zápis do proudu
V této jednohubce budete mít za úkol naimplementovat jednoduchou funkci, write_stats, která bere dva argumenty, sadu dat a výstupní proud, a do výstupního proudu zapíše 3 statistické údaje o datech: minimum, maximum a průměr.

Údaje musí být zapsány ve specifickém formátu, například pro vstupní data “1.23, 5.44, -23” by výstup vypadal takto:

min: -23.00
max: 5.44
mean: -5.44
Všimněte si, že čísla jsou vždy vypsaná se dvěma desetinnými čísly.

Projekt s testy najdete zde.

Do Brute odevzdávejte pouze Váš soubor tiny-00.cpp s vaší implementací funkce write_stats.

Rady
Formátovací manipulátory, které budete potřebovat, najdete v jedné ze dvou hlaviček:

iomanip
ios
Zároveň vás může zajímat šablona numeric_limits, přes kterou můžete získat informace o největší možné hodnotě typu a další podobné informace.