//
// Created by pc00520 on 12/1/2022.
//

#include <vector>
#include <fstream>
#include <random>
#include "fileManager.h"

int get_random_int() {
    static std::mt19937 mt{std::random_device{}()};
    static std::uniform_real_distribution<> dist(-2147483647, 2147483647);
    return (int) dist(mt);
}

int fileManager::readDataFromFile(const std::string &fileName, int *&array, int &size) {
    std::ifstream file(fileName);
    if (!file.is_open()) {
        return 1;
    }
    std::string line;
    std::vector<int> buffer;
    while (std::getline(file, line)) {
        buffer.push_back(std::stoi(line));
    }
    file.close();
    size = buffer.size();
    array = new int[size];
    for (int i = 0; i < size; ++i) {
        array[i] = buffer[i];
    }
    return 0;
}

int fileManager::writeStreamToFile(const std::string &filename, int *&array, int size) {
    std::ofstream file(filename, std::ios::trunc);
    if (!file.is_open()) {
        return 1;
    }
    for (int i = 0; i < size; ++i) {
        file << array[i] << "\n";
    }
    file.close();
    return 0;
}

int fileManager::generateUnsortedFile(const std::string &filename, int size) {
    std::ofstream file(filename, std::ios::trunc);
    if (!file.is_open()) {
        return 1;
    }
    for (int i = 0; i < size; ++i) {
        file << get_random_int() << "\n";
    }
    file.close();
    return 0;
}


int fileManager::compareTwoFiles(const std::string &filename1, const std::string &filename2) {
    std::ifstream file1(filename1);
    std::ifstream file2(filename2);
    std::string line1;
    std::string line2;
    if (!file1.is_open() || !file2.is_open()) {
        return 1;
    }
    while (!file1.eof() && !file2.eof()) {
        std::getline(file1, line1);
        std::getline(file2, line2);
        if (line1 != line2) {
            file1.close();
            file2.close();
            return 2;
        }
    }
    file1.close();
    file2.close();
    return 0;
}
