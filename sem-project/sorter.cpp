//
// Created by pc00520 on 11/30/2022.
//
#include "sorter.h"

//sort numbers on interval <lowIdx, highIdx> by pivot to smaller set (including pivot) then pivot & bigger set then pivot
int sorter::partition(int *array, const int lowIdx, const int highIdx, const int pivot) {
    int buffer;
    int i = lowIdx;
    for (int j = lowIdx; j <= highIdx; ++j) {
        if (array[j] <= pivot) {
            buffer = array[j];
            array[j] = array[i];
            array[i] = buffer;
            i++;
        }
    }
    return i - 1;
}

//takes two intervals of numbers <lowIdx, midIdx> & <lowIdx+1, highIdx> & merge them together sorted.
void sorter::merge(int *array, const int lowIdx, const int highIdx, const int midIdx) {
    int i = lowIdx;
    int j = midIdx + 1;
    int k = 0;
    int buffer[highIdx - lowIdx + 1];
    while (i <= midIdx && j <= highIdx) {
        buffer[k] = (array[i] < array[j]) ? array[i++] : array[j++];
        k++;
    }
    while (i <= midIdx) {
        buffer[k] = array[i];
        k++;
        i++;
    }
    while (j <= highIdx) {
        buffer[k] = array[j];
        k++;
        j++;
    }
    for (int l = lowIdx; l <= highIdx; ++l) {
        array[l] = buffer[l - lowIdx];
    }
}

//simple implementation of bubblesort
void sorter::bubbleSort(int *array, int size) {
    int buffer;
    for (int i = 0; i < size; i++) {
        for (int j = i + 1; j < size; j++) {
            if (array[j] < array[i]) {
                buffer = array[i];
                array[i] = array[j];
                array[j] = buffer;
            }
        }
    }
}

//recursive implementation of quicksort algorithm
void sorter::quickSort(int *array, int lowIdx, int highIdx) {
    if (lowIdx < highIdx) {
        int pivot = array[highIdx];
        int pivotIdx = partition(array, lowIdx, highIdx, pivot);
        quickSort(array, lowIdx, pivotIdx - 1);
        quickSort(array, pivotIdx + 1, highIdx);
    }
}

//recursive implementation of mergesort algorithm
void sorter::mergeSort(int *array, const int lowIdx, const int highIdx) {
    if (lowIdx < highIdx) {
        int mid = (lowIdx + highIdx) / 2;
        mergeSort(array, lowIdx, mid);
        mergeSort(array, mid + 1, highIdx);
        merge(array, lowIdx, highIdx, mid);
    }
}

void sorter::quickSort(int *array, int size) {
    return quickSort(array, 0, size - 1);
}

void sorter::mergeSort(int *array, int size) {
    return mergeSort(array, 0, size - 1);
}





