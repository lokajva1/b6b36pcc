#include <iostream>
#include <cstring>
#include <chrono>
#include "fileManager.h"
#include "sorter.h"
#include "printer.h"

void compareFiles();

void sortingComparator(const int *arrayBuffer, int &size);

int main(int argc, char **argv) {
    int *arrayBuffer = nullptr;
    int size = 0;
    std::string command;
    std::string stringBuffer;
    int numbBuffer;

//    user inputs check
    if (argc == 2 && std::strcmp(argv[1], "--help") == 0) {
        printer::manual(std::cout);
        return 0;
    }

    if (argc > 1) {
        printer::invalidArgument(std::cout);
        return 1;
    }

//    main livecycle of application that's read command from console and do action
    printer::custom(std::cout, "Hello there,\nplease enter command:");
    do {
        std::cin >> command;
//        check that input string from console is exactly one letter
        if (command.length() != 1) {
            printer::invalidArgument(std::cout);
            continue;
        }
        switch (command[0]) {
            case 'a':
                printer::enterFilename(std::cout);
                std::cin >> stringBuffer;
                if (fileManager::readDataFromFile(stringBuffer, arrayBuffer, size) == 0) {
                    printer::successful(std::cout);
                } else {
                    printer::operationFailed(std::cout);
                }
                break;
            case 'b':
                if (arrayBuffer != nullptr) {
                    sorter::bubbleSort(arrayBuffer, size);
                    printer::successful(std::cout);
                } else {
                    printer::operationFailed(std::cout);
                }
                break;
            case 'c':
                if (arrayBuffer != nullptr) {
                    sorter::quickSort(arrayBuffer, size);
                    printer::successful(std::cout);
                } else {
                    printer::operationFailed(std::cout);
                }
                break;
            case 'd':
                if (arrayBuffer != nullptr) {
                    sorter::mergeSort(arrayBuffer, size);
                    printer::successful(std::cout);
                } else {
                    printer::operationFailed(std::cout);
                }
                break;
            case 'e':
                if (arrayBuffer != nullptr) {
                    sortingComparator(arrayBuffer, size);
                } else {
                    printer::operationFailed(std::cout);
                }
                break;
            case 'f':
                printer::logArray(std::cout, arrayBuffer, size);
                break;
            case 'g':
                printer::enterFilename(std::cout);
                std::cin >> stringBuffer;
                if (fileManager::writeStreamToFile(stringBuffer, arrayBuffer, size) == 0) {
                    printer::successful(std::cout);
                } else {
                    printer::operationFailed(std::cout);
                }
                break;
            case 'h':
                compareFiles();
                break;
            case 'i':
                printer::enterFilename(std::cout);
                std::cin >> stringBuffer;
                printer::custom(std::cout,"Write how many numbers you have to generate");
                std::cin >> numbBuffer;
                if (fileManager::generateUnsortedFile(stringBuffer, numbBuffer) == 0) {
                    printer::successful(std::cout);
                } else {
                    printer::operationFailed(std::cout);
                }
                break;
            case 'j':
                printer::custom(std::cout, "Good by");
                break;
            default:
                printer::invalidArgument(std::cout);
                break;
        }
    } while (command != "j");

    delete[] arrayBuffer;
    return 0;
}

void compareFiles() {
    std::string stringBuffer;
    std::string fileNames[2];
    for (std::string &fileName: fileNames) {
        printer::enterFilename(std::cout);
        std::cin >> stringBuffer;
        fileName = stringBuffer;
    }
    if (fileManager::compareTwoFiles(fileNames[0], fileNames[1]) == 0) {
        printer::custom(std::cout, "Files are identical");
    } else {
        printer::custom(std::cout, "Files are not identical");
    }
}

template<typename TimePoint>
std::chrono::milliseconds to_ms(TimePoint tp) {
    return std::chrono::duration_cast<std::chrono::milliseconds>(tp);
}

//compare all three sorting algorythm and print result to console
void sortingComparator(const int *arrayBuffer, int &size) {
    int *arrayBufferDeepCopy = new int[size];
    std::string name;
    for (int i = 0; i < 3; ++i) {
        for (int j = 0; j < size; ++j) {
            arrayBufferDeepCopy[j] = arrayBuffer[j];
        }
        auto start = std::chrono::high_resolution_clock::now();
        switch (i) {
            case 0:
                name = "Bubblesort";
                sorter::bubbleSort(arrayBufferDeepCopy, size);
                break;
            case 1:
                name = "Quicksort";
                sorter::quickSort(arrayBufferDeepCopy, size);
                break;
            case 2:
                name = "Mergesort";
                sorter::mergeSort(arrayBufferDeepCopy, size);
                break;
        }
        auto end = std::chrono::high_resolution_clock::now();
        printer::timeNeeded(std::cout, name, to_ms(end - start).count());
    }
    delete [] arrayBufferDeepCopy;
}
