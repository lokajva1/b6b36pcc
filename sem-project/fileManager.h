//
// Created by pc00520 on 12/1/2022.
//

#ifndef SEM_PROJECT_FILEMANAGER_H
#define SEM_PROJECT_FILEMANAGER_H

#include <string>

//manager for data operations all operations are implemented just for int
class fileManager {
private:
public:
    static int readDataFromFile(const std::string &filename, int *&array, int &size);

    static int writeStreamToFile(const std::string &filename, int *&array, int size);

    static int generateUnsortedFile(const std::string &filename, int size);

    static int compareTwoFiles(const std::string &filename1, const std::string &filename2);
};


#endif //SEM_PROJECT_FILEMANAGER_H
