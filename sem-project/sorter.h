//
// Created by pc00520 on 11/30/2022.
//

#ifndef SEM_PROJECT_SORTER_H
#define SEM_PROJECT_SORTER_H

//implementation of sorting algorythms all operations are implemented just for int
class sorter {
private:
    static int partition(int *array, int lowIdx, int highIdx, int pivot);

    static void merge(int *array, int lowIdx, int highIdx, int midIdx);

    static void quickSort(int *array, int lowIdx, int highIdx);

    static void mergeSort(int *array, int lowIdx, int highIdx);

public:
    static void bubbleSort(int *array, int size);

    static void quickSort(int *array, int size);

    static void mergeSort(int *array, int size);
};

#endif //SEM_PROJECT_SORTER_H
