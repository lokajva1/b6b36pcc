//
// Created by pc00520 on 12/1/2022.
//

#ifndef SEM_PROJECT_PRINTER_H
#define SEM_PROJECT_PRINTER_H

#include <iostream>

//manager for print output for stream
class printer {
private:
public:
    static void logArray(std::ostream &stream, const int *array, int size);

    static void manual(std::ostream &stream);

    static void custom(std::ostream &stream, const std::string &text);

    static void invalidArgument(std::ostream &stream);

    static void successful(std::ostream &stream);

    static void operationFailed(std::ostream &stream);

    static void enterFilename(std::ostream &stream);

    static void timeNeeded(std::ostream &stream, const std::string &name, int duration);

};


#endif //SEM_PROJECT_PRINTER_H
