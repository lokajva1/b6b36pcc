//
// Created by pc00520 on 12/1/2022.
//

#include "printer.h"

void printer::logArray(std::ostream &stream, const int *array, int size) {
    stream << "Size is: " << size << "\nData are: \n";
    for (int i = 0; i < size - 1; ++i) {
        stream << array[i] << ", ";
    }
    stream << array[size - 1] << "\n";
}

void printer::manual(std::ostream &stream) {
    stream << "a - load file from system takes name of input file as a parameter from console\n"
           << "b - do bubble sort\n"
           << "c - do quick sort\n"
           << "d - do merge sort\n"
           << "e - compare sorting algorithm on loaded series and print result into console\n"
           << "f - print current array to console\n"
           << "g - save result to file takes name of file and create it in filesystem\n"
           << "h - compare two files from filesystem\n"
           << "i - generate unsorted set\n"
           << "j - exit application\n";
}

void printer::custom(std::ostream &stream, const std::string &text) {
    stream << text << "\n";
}

void printer::invalidArgument(std::ostream &stream) {
    stream << "Invalid input arguments\n";
}

void printer::successful(std::ostream &stream) {
    stream << "Successful done\n";
}

void printer::operationFailed(std::ostream &stream) {
    stream << "Operation failed\n";
}

void printer::enterFilename(std::ostream &stream) {
    stream << "Write file name (including .txt)\n";
}

void printer::timeNeeded(std::ostream &stream, const std::string &name, int duration) {
    stream << "Algorithm " << name << " needed " << duration << " ms to sort array.\n";
}





