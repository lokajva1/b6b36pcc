#include <queue>
#include "crusoe.hpp"

//vertex class

vertex::vertex() {}

vertex::vertex(std::string str, int x, int y, std::string col) {
    name = str;
    xy = std::make_pair(x, y);
    c_forward = col;
}

bool vertex::add_neighbour(size_t vv, const std::string &col) {
    neighbours.emplace_back(vv, col);
    return true;
}

std::vector<std::pair<size_t, std::string>> vertex::get_neighbour() const {
    return neighbours;
}

std::pair<int, int> vertex::get_xy() const {
    return xy;
}

void vertex::set_color(const std::string &col) {
    c_forward = col;
}

std::string vertex::get_color() const {
    return c_forward;
}

void vertex::set_edge_color(size_t vv, const std::string &col) {
    for (auto &neighbour: neighbours) {
        if (neighbour.first == vv) {
            neighbour.second = col;
        }
    }
}

std::string vertex::get_edge_color(size_t vv) {
    for (auto &neighbour: neighbours) {
        if (neighbour.first == vv) {
            return neighbour.second;
        }
    }
    return "#FFFFFF";
}

//graph class

void graph::set_vertex_color(size_t v1, const std::string &col) {
    if (v1 < vertices.size()) {
        vertices[v1].set_color(col);
    }
}

void graph::add_vertex(int x, int y, const std::string &col) {
    vertex newVertex(std::to_string(num_elem), x, y, col);
    vertices.push_back(newVertex);
    num_elem++;
}

void graph::add_edge(size_t v1, size_t v2, const std::string &col) {
    if (!is_edge(v1, v2)) {
        vertices[v1].add_neighbour(v2, col);
        vertices[v2].add_neighbour(v1, col);
    }
}

bool graph::is_edge(size_t v1, size_t v2) const {
//    index check
    if (v1 > num_elem || v2 > num_elem) {
        return false;
    }
    auto v1Neighbour = vertices[v1].get_neighbour();
    for (const auto &neighbor: v1Neighbour) {
        if (neighbor.first == v2) {
            return true;
        }
    }
    return false;
}

std::string graph::edge_color(size_t v1, size_t v2) const {
    if (is_edge(v1, v2)) {
        auto edges = vertices[v1].get_neighbour();
        for (const auto &edge: edges) {
            if (edge.first == v2) {
                return edge.second;
            }
        }
    }
    return "#FFFFFF";
}

std::string graph::vertex_color(size_t v1) const {
    if (v1 < vertices.size()) {
        return vertices[v1].get_color();
    }
    return "#FFFFFF";
}

void graph::set_edge_color(size_t v1, size_t v2, const std::string &col) {
    if (is_edge(v1, v2)) {
        vertices[v1].set_edge_color(v2, col);
        vertices[v2].set_edge_color(v1, col);
    }
}

bool graph::empty() const {
    return vertices.empty();
}

size_t graph::size() const {
    return num_elem;
}

size_t graph::num_edge() const {
    size_t vertexNumb = 0;
    for (const auto &vertex: vertices) {
        vertexNumb += vertex.get_neighbour().size();
    }
    return vertexNumb / 2;
}

vertex graph::get_vertex(size_t num) const {
    return vertices[num];
}

void graph::is_achievable(size_t from, std::vector<size_t> &achieved) {
    auto fromNeighbour = get_vertex(from).get_neighbour();
    if (std::find(std::begin(achieved), std::end(achieved), from) != std::end(achieved) == 0) {
        achieved.push_back(from);
    }
    for (const auto &neighbour: fromNeighbour) {
        if (std::find(std::begin(achieved), std::end(achieved), neighbour.first) != std::end(achieved)) {
            continue;
        } else {
            achieved.push_back(neighbour.first);
            is_achievable(neighbour.first, achieved);
        }
    }
}

void graph::color_component(std::vector<size_t> cmp, const std::string &col) {
    for (auto idx: cmp) {
        set_vertex_color(idx, col);
        auto currentNeighbour = get_vertex(idx).get_neighbour();
        for (const auto &neighbour: currentNeighbour) {
            set_edge_color(idx, neighbour.first, col);
        }
    }
}

std::vector<size_t> graph::path(size_t v1, size_t v2) {
//    current node / previous node
    std::vector<std::pair<size_t, size_t>> graphPath;
    std::queue<size_t> queue;

    std::vector<bool> visited;
    visited.resize(vertices.size(),false);

    visited[v1] = true;
    queue.push(v1);

    size_t vertexIdx;

    while(!queue.empty()){
        vertexIdx = queue.front();
        queue.pop();

        if (vertexIdx == v2) {
//                build shortestPath
            std::vector<size_t> shortestPath;
            size_t currentValue = vertexIdx;
            while (currentValue != v1) {
                shortestPath.push_back(currentValue);
                for (auto path: graphPath) {
                    if (path.first == currentValue) {
                        currentValue = path.second;
                        break;
                    }
                }
            }
            shortestPath.push_back(v1);
            reverse(shortestPath.begin(), shortestPath.end());
            return shortestPath;
        }

        auto neighbourArr = get_vertex(vertexIdx).get_neighbour();

        for (const auto& neighbour: neighbourArr)
        {
            if (!visited[neighbour.first])
            {
                visited[neighbour.first] = true;
                queue.push(neighbour.first);
                graphPath.emplace_back(neighbour.first, vertexIdx);
            }
        }
    }

    return std::vector<size_t>();
}

void graph::color_path(std::vector<size_t> pth, const std::string &col) {
    size_t previousVertex = pth.at(0);
    for (auto vertex: pth) {
        if (vertex == previousVertex) {
            continue;
        }
        set_edge_color(previousVertex, vertex, col);
        previousVertex = vertex;
    }
}

//graph_comp

graph::graph_comp::graph_comp(graph &gg) : gg(gg) {
    std::vector<size_t> achieved;
    std::vector<size_t> achievable;
    for (size_t i = 0; i < gg.vertices.size(); i++) {
//        component already exist
        if (std::any_of(achieved.begin(), achieved.end(), [i](const size_t &p) { return p == i; })) {
            continue;
        }
//        found other component
        gg.is_achievable(i, achievable);
        components.push_back(achievable);
        for (auto a: achievable) {
            achieved.push_back(a);
        }
        achievable.clear();
    }
}

void graph::graph_comp::color_componennts() {
    std::vector<std::string> colors{"red", "olive", "orange", "lightblue", "yellow", "pink", "cyan", "purple", "brown",
                                    "magenta"};
    int colourIdx = 0;
    std::string colour;
    for (const auto &component: components) {
        if (colourIdx == 10) {
            colourIdx = 0;
        }
        colour = colors[colourIdx];
        gg.color_component(component, colour);
        colourIdx++;
    }
}

size_t graph::graph_comp::count() const {
    return components.size();
}

size_t graph::graph_comp::count_without_one() const {
    size_t componentCnt = 0;
    for (const auto &component: components) {
        if (component.size() > 1) {
            componentCnt++;
        }
    }
    return componentCnt;
}

size_t graph::graph_comp::max_comp() const {
    auto maxComponent = components[0];
    size_t maxComponentIdx = 0;
    size_t idx = 0;
    for (const auto &component: components) {
        if (component.size() > maxComponent.size()) {
            maxComponent = component;
            maxComponentIdx = idx;
        }
        idx++;
    }
    return maxComponentIdx;
}

size_t graph::graph_comp::size_of_comp(size_t i) const {
    return components[i].size();
}

std::vector<size_t> graph::graph_comp::get_component(size_t i) const {
    return components[i];
}

bool graph::graph_comp::same_comp(size_t v1, size_t v2) const {
    std::vector<size_t> achievable;
    gg.is_achievable(v1, achievable);
    return std::any_of(achievable.begin(), achievable.end(), [v2](const size_t &p) { return p == v2; });
}

//graph_fence

graph::graph_fence::graph_fence(graph &gg, size_t vv, size_t distance) : gg(gg) {
    std::vector<size_t> achievable;
    gg.is_achievable(vv, achievable);
    for (auto vertex : achievable){
        auto pathLength = gg.path(vv, vertex).size() - 1;
        if (pathLength <= distance){
            fence.push_back(vertex);
        }
    }
}

void graph::graph_fence::color_fence(const std::string &col) {
    for (auto vertex : fence){
        gg.set_vertex_color(vertex, col);
    }
}

size_t graph::graph_fence::count_stake() const {
    return fence.size();
}

size_t graph::graph_fence::get_stake(size_t i) const {
    if (fence.size() > i){
        return fence[i];
    }
    return -1;
}