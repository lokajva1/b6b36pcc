U1: Webbův vesmírný dalekohled
Webbův vesmírný dalekohled obíhá po halo orbitě a zaznamenává snímky blízkého i dalekého vesmíru zejména v infračerveném spektru. Odstartoval ze Země 25. prosince 2021 a po sestavení ve vesmíru začal posílat na Zemi fotografie. Data z dalekohledu jsou pro přenos na Zemi šifrována a pro správné zobrazení musí být opět dešifrována.

Šifrovací algoritmus je poměrně výpočtově náročný. Úkolem je napsat program, který dešifruje snímky získané z Webbova vesmírného dalekohledu a obrázky uloží ve formátu PPM pro správné zobrazení.



Co si v tomto úkolu vyzkoušíte
Práce s jednorozměrným polem std::vector
Implementaci dvourozměrného pole pomocí pole jednorozměrného
Práce s testy a možnosti testování programů v C++
Poznáte možnosti ukládání obrázků jako množiny barevných pixelů
Zadání
Seznam metod, které musíte implementovat najdete v hlavičce telescop.hpp. Co jednotlivé metody mají dělat pak najdete v téže hlavičce, ve formátu Doxygen komentářů.

Archiv s testy, hlavičkou telescope.hpp a CMakeLists.txt najdete zde.

Co odevzdat?
Jeden nebo více .cpp souborů (obvykle telescope.cpp), které implementují funkce deklarované v souboru telescop.hpp tak, aby testy procházely a neztrácela se paměť. Při práci na úkolu soubor telescope.hpp neměňte.

Hodnocení
Sekce	Body
Základní práce s polem (stage1)	2
Práce s polem bajtů, načtení pole + operátory (stage2)	4
Dekódování obrázků (stage3)	3
Komplexní testy	1
Celkem	10
Postup řešení
Obrázek je formátu PPM je v obecnosti dvourozměrné pole jednotlivých obrazových bodů. Pro dvourozměrné pole využíváme v úloze jednorozměrnou strukturu std::vector, ve které jsou řádky matice řazeny za sebou. Na začátku je potřeba dobře si rozmyslet práci s indexy jednorozměrného pole reprezentujícího dvourozměrnou matici. K tomu slouží Stage1, kde se pracuje s polem typu int. V následujících úrovních (Stage2 a Stage3) je pak využito pole typu unsigned char.

Doporučujeme postupně implementovat jednotlivé funkce definované v souboru telescop.hpp. Požadovaná funkčnost je popsána v komentářích. Současně jsou pro každou funkci definovány testy, které ověřují správnou funkčnost definovaných funkcí. Testy lze spouštět I samostatně podle toho, kterou část úlohy zrovna řešíte.

Většina testů na ukládá výsledek do souboru. Ten najdete ve složce, do níž projekt překládáte a kde ho spouštíte. Vykreslené obrázky vám mohou být nápomocné při řešení úlohy.

Doporučujeme úlohu řešit postupně, jak jsou jednotlivé funkce v zadání uvedeny. Každá další funkce předpokládá správnou funkčnost předchozích funkcí. Je možné některé funkce neodevzdat, vzhledem k jejich návaznosti se ale zbytečně připravujete o body.
