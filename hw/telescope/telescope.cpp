#include "telescope.hpp"
#include <sstream>
#include <iomanip>

class Matrix {
private:
    size_t y = 0;
    size_t x = 0;
    std::vector<int> data;
public:
    Matrix() {}

    Matrix(const std::pair<size_t, size_t> &m_size, const std::vector<int> &data) {
        y = m_size.first;
        x = m_size.second;
        Matrix::data = data;
    }

    void incX() {
        x++;
    }

    size_t getY() const {
        return y;
    }

    void setY(size_t y) {
        Matrix::y = y;
    }

    size_t getX() const {
        return x;
    }

    const std::vector<int> &getData() const {
        return data;
    }

    void insertData(const std::vector<int> &dataIns) {
        for (int numb: dataIns) {
            data.push_back(numb);
        }
    }

    std::string print() {
        size_t max_numb_length = 0;
        size_t numb_length;
        std::string s_out;

//        check of empty matrix
        if (x == 0 || y == 0) {
            return "";
        }

//        determinate longest string
        for (int numb: data) {
            numb_length = std::to_string(numb).size();
            if (numb_length > max_numb_length) {
                max_numb_length = numb_length;
            }
        }

//        generate ---... part on top
        size_t row_width = (max_numb_length + 3) * x + 1;
        for (size_t i = 0; i < row_width; ++i) {
            s_out += '-';
        }

//        generate part with numbers
        size_t x_position = 0;
        for (int numb: data) {
//            end of line
            if (x_position % x == 0) {
                s_out += "\n|";
            }
//            generate ' ' before number
            numb_length = std::to_string(numb).size();
            for (size_t i = 0; i < max_numb_length - numb_length + 1; ++i) {
                s_out += ' ';
            }
            s_out += std::to_string(numb) + " |";

//            en end of number output print '\n'
            if (x_position == data.size() - 1) {
                s_out += '\n';
            }

            x_position++;
        }

        for (size_t i = 0; i < row_width; ++i) {
            s_out += '-';
        }

        return s_out + '\n';
    }

};

//own methods headers
std::pair<size_t, std::vector<int>> parse_int_matrix_row(const std::string &row_string);

Matrix parse_int_matrix_universal(std::istream &in);

//entered methods
//STAGE 1
std::pair<size_t, size_t> parse_matrix(std::istream &in) {
    Matrix matrix = parse_int_matrix_universal(in);

    return std::make_pair(matrix.getX(), matrix.getY());
}

std::vector<int> parse_matrix(std::istream &in, const std::pair<size_t, size_t> &m_size) {
    Matrix matrix = parse_int_matrix_universal(in);

    return matrix.getData();
}

void print_matrix(std::ostream &out, const std::pair<size_t, size_t> &m_size, const std::vector<int> &vec) {
    Matrix matrix(m_size, vec);

    out << matrix.print();
}

//own methods STAGE1
Matrix parse_int_matrix_universal(std::istream &in) {
    Matrix matrix;
    std::string str_buf;
    std::stringstream str;
    std::pair<size_t, std::vector<int>> parsed_row;
    size_t y;

//    make vectors of lines
    while (getline(in, str_buf)) {
        parsed_row = parse_int_matrix_row(str_buf);
        y = parsed_row.first;

//        first cycle skip check
        if (matrix.getY() == 0) {
            matrix.setY(y);
            matrix.setY(y);
        }

//        invalid data check
        if (matrix.getY() != y) {
            throw std::invalid_argument("Invalid row in matrix.");
        }
        matrix.insertData(parsed_row.second);
        matrix.incX();
    }

//    m_size as output parameter
    return matrix;
}

std::pair<size_t, std::vector<int>> parse_int_matrix_row(const std::string &row_string) {
    std::vector<int> matrix;
    int number;
    size_t rows_count = 0;
    std::stringstream str;
    str.str(row_string);

    while (str >> number) {
        matrix.push_back(number);
        rows_count++;
    }

    return std::make_pair(rows_count, matrix);
}

//STAGE 2
void deep_copy_matrix(std::vector<unsigned char> &source, std::vector<unsigned char> &target);

std::vector<unsigned char>
rotate_matrix_to_ninetieth_deg_right(const std::pair<size_t, size_t> &m_size, std::vector<unsigned char> &vec);

std::vector<unsigned char> parse_stream(std::istream &in, const std::pair<size_t, size_t> &m_size) {
    std::vector<unsigned char> matrix;
    size_t x = m_size.first;
    size_t y = m_size.second;
    char buffer;
    while (in.get(buffer)) {
        matrix.push_back(static_cast<unsigned char>(buffer));
    }

    if (y * x != matrix.size()) {
        throw std::invalid_argument("Invalid number of input elements.");
    }

    return matrix;
}

void rotate_down(const std::pair<size_t, size_t> &m_size, std::vector<unsigned char> &vec) {
    std::vector<unsigned char> rotated_matrix;
    size_t x = m_size.second;

//    last row to new vector
    for (size_t i = vec.size() - x; i < vec.size(); ++i) {
        rotated_matrix.push_back(vec[i]);
    }

//    move rest of matrix one row down
    for (size_t i = 0; i < vec.size() - x; ++i) {
        rotated_matrix.push_back(vec[i]);
    }

    deep_copy_matrix(rotated_matrix, vec);
}

void rotate_down(const std::pair<size_t, size_t> &m_size, std::vector<unsigned char> &vec, int step) {
    std::vector<unsigned char> rotated_matrix;
    size_t optimalized_step;
    size_t y = m_size.first;
    size_t x = m_size.second;

//    cast negative to positive
    if (step < 0) {
        int positive_numb = -1 * step;
        optimalized_step = positive_numb;
    } else {
        optimalized_step = step;
    }

//    optimalization of rotation down
    while (optimalized_step > y) {
        optimalized_step -= y;
    }

//    recalculate number if step was negative
    if (step < 0) {
        optimalized_step = y - optimalized_step;
    }

//    bottom row to new vector
    for (size_t i = vec.size() - x * optimalized_step; i < vec.size(); ++i) {
        rotated_matrix.push_back(vec[i]);
    }

//    move rest of matrix down
    for (size_t i = 0; i < vec.size() - x * optimalized_step; ++i) {
        rotated_matrix.push_back(vec[i]);
    }

    deep_copy_matrix(rotated_matrix, vec);
}

void rotate_right(const std::pair<size_t, size_t> &m_size, std::vector<unsigned char> &vec) {
    std::pair<size_t, size_t> reversed_m_size = std::make_pair(m_size.second, m_size.first);

    std::vector<unsigned char> rotated_matrix = rotate_matrix_to_ninetieth_deg_right(m_size, vec);
    rotate_down(reversed_m_size, rotated_matrix);

//    rater 3 times to right then one to left
    rotated_matrix = rotate_matrix_to_ninetieth_deg_right(reversed_m_size, rotated_matrix);
    rotated_matrix = rotate_matrix_to_ninetieth_deg_right(m_size, rotated_matrix);
    rotated_matrix = rotate_matrix_to_ninetieth_deg_right(reversed_m_size, rotated_matrix);

    deep_copy_matrix(rotated_matrix, vec);
}

void rotate_right(const std::pair<size_t, size_t> &m_size, std::vector<unsigned char> &vec, int step) {
    std::pair<size_t, size_t> reversed_m_size = std::make_pair(m_size.second, m_size.first);

    std::vector<unsigned char> rotated_matrix = rotate_matrix_to_ninetieth_deg_right(m_size, vec);
    rotate_down(reversed_m_size, rotated_matrix, step);

//    rater 3 times to right then one to left
    rotated_matrix = rotate_matrix_to_ninetieth_deg_right(reversed_m_size, rotated_matrix);
    rotated_matrix = rotate_matrix_to_ninetieth_deg_right(m_size, rotated_matrix);
    rotated_matrix = rotate_matrix_to_ninetieth_deg_right(reversed_m_size, rotated_matrix);

    deep_copy_matrix(rotated_matrix, vec);
}

void swap_points(const std::pair<size_t, size_t> &m_size, std::vector<unsigned char> &vec, const Point &p1,
                 const Point &p2) {
    unsigned char swap_buffer;
    size_t y = m_size.first;
    size_t x = m_size.second;
    if (p1.y >= y || p1.x >= x || p2.y >= y || p2.x >= x) {
        throw std::invalid_argument("Swap point is out of range.");
    }

    size_t p1_index = p1.y * x + p1.x;
    size_t p2_index = p2.y * x + p2.x;

    swap_buffer = vec[p1_index];
    vec[p1_index] = vec[p2_index];
    vec[p2_index] = swap_buffer;
}

void
swap_points(const std::pair<size_t, size_t> &m_size, std::vector<unsigned char> &vec, const Point &p1, const Point &p2,
            const Point &delta) {
    size_t y = m_size.first;
    size_t x = m_size.second;
    size_t delta_x_after_sub = delta.x - 1;
    size_t delta_y_after_sub = delta.y - 1;

    if (p1.y + delta_y_after_sub >= y || p1.x + delta_x_after_sub >= x || p2.y + delta_y_after_sub >= y ||
        p2.x + delta_x_after_sub >= x) {
        throw std::invalid_argument("Swap point is out of range.");
    }

    size_t loop_y = 0;
    size_t loop_x = 0;
    std::vector<Point> p1_matrix;
    std::vector<Point> p2_matrix;
    bool is_in_p1;
    bool is_in_p2;
    for (size_t i = 0; i < vec.size(); ++i) {
//        recalculate coordinates
        if (loop_x >= x) {
            loop_y++;
            loop_x = 0;
        }

//        calc position in matrix
        is_in_p1 = loop_y <= p1.y + delta_y_after_sub && loop_x <= p1.x + delta_x_after_sub
                   && loop_y >= p1.y && loop_x >= p1.x;
        is_in_p2 = loop_y <= p2.y + delta_y_after_sub && loop_x <= p2.x + delta_x_after_sub
                   && loop_y >= p2.y && loop_x >= p2.x;

//        save point of p1
        if (is_in_p1) {
            p1_matrix.emplace_back(Point(loop_x, loop_y));
        }

//        save point of p2
        if (is_in_p2) {
            p2_matrix.emplace_back(Point(loop_x, loop_y));
        }

//        check overflow rectangles
        if (is_in_p1 && is_in_p2) {
            throw std::invalid_argument("Rectangles are overflowing.");
        }

        loop_x++;
    }

//    swap points
    for (size_t i = 0; i < p1_matrix.size(); ++i) {
        swap_points(m_size, vec, p1_matrix[i], p2_matrix[i]);
    }
}

//own methods STAGE2
void deep_copy_matrix(std::vector<unsigned char> &source, std::vector<unsigned char> &target) {
    target.clear();
    for (unsigned char numb: source) {
        target.push_back(numb);
    }
}

std::vector<unsigned char>
rotate_matrix_to_ninetieth_deg_right(const std::pair<size_t, size_t> &m_size, std::vector<unsigned char> &vec) {
    std::vector<unsigned char> rotated_matrix;
    size_t y = m_size.first;
    size_t x = m_size.second;
    size_t read_index;
    size_t row_move = 1;
    size_t column_move = 0;

//    copy each element in vector to rotated matrix
    for (size_t i = 0; i < vec.size(); ++i) {
        if (row_move > y) {
            row_move = 1;
            column_move++;
        }
//        calc index from matrix
        read_index = vec.size() - x * row_move + column_move;
        rotated_matrix.push_back(vec[read_index]);
        row_move++;
    }

    return rotated_matrix;
}

void decode_picture(const std::string& file, const std::pair<size_t, size_t>& m_size, std::vector<unsigned char>& vec){
    std::ifstream f;
    f.open(file);
    std::string row_string;
    std::stringstream str;
    char command;
    std::vector<int> aditional_coordinates;

//    read by line from file
    while(getline(f, row_string)){
        int numb_buffer;
        str.str(row_string);

//        read command shortcut
        str >> command;

//        read aditional number after shortcut if is possible
        while (str >> numb_buffer){
            aditional_coordinates.push_back(numb_buffer);
        }

//        simple commands
        if (aditional_coordinates.empty()){
            switch (command){
                case 'r':
                    rotate_right(m_size, vec);
                    break;
                case 'l':
                    rotate_right(m_size, vec, -1);
                    break;
                case 'd':
                    rotate_down(m_size, vec);
                    break;
                case 'u':
                    rotate_down(m_size, vec, -1);
                    break;
            }
        } else {
//            commands with parameters
            switch (command){
                case 'r':
                    rotate_right(m_size, vec, aditional_coordinates.at(0));
                    break;
                case 'l':
                    rotate_right(m_size, vec, -aditional_coordinates.at(0));
                    break;
                case 'd':
                    rotate_down(m_size, vec, aditional_coordinates.at(0));
                    break;
                case 'u':
                    rotate_down(m_size, vec, -aditional_coordinates.at(0));
                    break;
                case 's':
                    Point p1 = Point(aditional_coordinates.at(0), aditional_coordinates.at(1));
                    Point p2 = Point(aditional_coordinates.at(2), aditional_coordinates.at(3));
                    if (aditional_coordinates.size() < 5){
                        swap_points(m_size, vec, p1, p2);
                    } else {
                        Point delta = Point(aditional_coordinates.at(4), aditional_coordinates.at(5));
                        swap_points(m_size, vec, p1, p2, delta);
                    }
                    break;
            }
        }

        aditional_coordinates.clear();
        str.clear();
    }

    f.close();
}